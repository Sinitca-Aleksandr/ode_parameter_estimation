# -*- coding: utf-8 -*-
"""
MIT License

Copyright (c) 2018 Digiratory, Sinitca-Aleksandr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import pylab as pp
import numpy as np
from scipy import integrate, interpolate
from scipy import optimize
from scipy.integrate import odeint


class parameter_estimator():
    def __init__(self, x_data, y_data, f, n_observed=None):
        self._x_data = x_data
        self._y_data = y_data
        self._f = f
        self._c = None
        self.n_observed = y_data.shape[1]

    def my_ls_func(self, x, teta):
        """
        Определение функции, возвращающей значения решения ДУ в
        процессе оценки параметров
        x заданные (временные) точки, где известно решение
        (экспериментальные данные)
        teta -- массив с текущим значением оцениваемых параметров.
        Первые self._y0_len элементов -- начальные условия,
        остальные -- параметры ДУ
        """
        # Для передачи функуии используем ламбда-выражение с подставленными
        # параметрами
        # Вычислим значения дифференциального уравления в точках "x"
        r = integrate.odeint(lambda y, t: self._f(y, t, teta[self._y0_len:]),
                             teta[0:self._y0_len], x)
        # Возвращаем только наблюдаемые переменные
        return r[:, 0:self.n_observed]

    def f_resid(self, p):
        """
        Функция для передачи в optimize.leastsq
        При дальнейших вычислениях значения, возвращаемые этой функцией,
        будут возведены в квадрат и просуммированы
        """
        delta = self._y_data - self.my_ls_func(self._x_data, p)
        return delta.flatten()  # Преобразуем в одномерный массив

    def estimate(self, y0, guess):
        """
        Произвести оценку параметров дифференциального уравнения с заданными
        начальными значениями параметров:
            y0 -- начальные условия ДУ
            guess -- параметры ДУ
        """

        # Сохраняем число начальных условий
        self._y0_len = len(y0)

        # Создаем вектор оцениваемых параметров,
        # включающий в себя начальные условия
        self._est_values = np.concatenate((y0, guess))

        # Решить оптимизационную задачу - решение в переменной c
        (c, kvg) = optimize.leastsq(self.f_resid, self._est_values)
        self._c = c
        # В возвращаемом значении разделяем начальные условия и параметры
        return c[self._y0_len:], c[0:self._y0_len]

    def calcODE(self, args, y0, x0=0, xEnd=10, nt=101):
        """
        Служебная функция для решения ДУ
        """
        t = np.linspace(x0, xEnd, nt)
        sol = odeint(self._f, y0, t, args)
        return sol, t

    def plot_result(self):
        """
        Построить графическое предстваление результутов оценки параметров
        """
        if self._c is None:
            print("Parameter is not estimated.")
            return

        sol, t = self.calcODE((self._c[self._y0_len:],),
                              self._c[0:self._y0_len],
                              min(self._x_data),
                              max(self._x_data))
        # Строим экспериментальные данные, каккрасные точки,
        # а результаты моделирования, как синюю линию
        pp.plot(self._x_data, self._y_data, '.r', t, sol, '-b')
        pp.xlabel('xlabel', {"fontsize": 16})
        pp.ylabel("ylabel", {"fontsize": 16})
        pp.legend(('data', 'fit'), loc=0)
        pp.show()

# -*- coding: utf-8 -*-
"""
MIT License

Copyright (c) 2018 Digiratory, Sinitca-Aleksandr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

"""
import numpy as np
from estimation import parameter_estimator

from scipy.integrate import odeint
import matplotlib.pyplot as plt


def ode(y, t, k):
    """
    Функция, рализующая систему ДУ маятника с трением
    """
    theta, omega = y
    b = k[0]
    c = k[1]
    dydt = [omega, -b*omega - c*np.sin(theta)]
    return dydt


def calcODE(args, y0, dy0, ts=10, nt=101):
    """
    Вспомогательная функция для получения решения систему ДУ
    """
    y0 = [y0, dy0]
    t = np.linspace(0, ts, nt)
    sol = odeint(ode, y0, t, args)
    return sol, t

# Зададим истинные значения параметров системы
b = 0.3
c = 5.0

args = ([b, c], )
y0 = 1
dy0 = 0
print("Real parameter: b = {}, c = {}".format(b, c))
print("Real initial condition: {} {}".format(y0, dy0))
sol, t = calcODE(args, y0, dy0)

guess = [0.2, 0.3]  # Начальные значения для параматров системы
y0 = [0, 1]  # Стартовые начальные значения для системы ДУ

estimator = parameter_estimator(t, sol, ode)
est_par = estimator.estimate(y0, guess)
# Построим графики результатов оценки параметров
estimator.plot_result()
print("Estimated parameter: b={}, c={}".format(est_par[0][0], est_par[0][1]))
print("Estimated initial condition: {}".format(est_par[1]))
